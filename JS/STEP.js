const tabNav = document.querySelector('.ul-nav-our-services');
const allTextContent = document.querySelectorAll('.tabs-paragraph-text');
const tabsImages = document.querySelectorAll('.tab-img');

// ---   ---   ---   TAB  OUR SERVICES   ---   ---   --- //

tabNav.addEventListener('click', function (event) {
    const tabMenu = document.querySelector('.active-tab');
    tabMenu.classList.remove('active-tab');
    tabMenu.classList.remove('start-decor');
    tabMenu.classList.remove('tabs-onclick');
    allTextContent.forEach(item => {
        item.classList[item.dataset.paragraph === event.target.dataset.tabnav ? 'add' : 'remove']('active-paragraph');
        item.classList[item.dataset.paragraph === event.target.dataset.tabnav ? 'remove' : 'add']('hide');
    });
    tabsImages.forEach(item => {
        item.classList[item.dataset.paragraph === event.target.dataset.tabnav ? 'add' : 'remove']('active-paragraph');
        item.classList[item.dataset.paragraph === event.target.dataset.tabnav ? 'remove' : 'add']('hide');
    });
    event.target.classList.add('active-tab');
    event.target.classList.add('tabs-onclick');
});


// ---   ---   ---   TAB AMAZING   ---   ---   --- //
const tabNavAmazing = document.querySelector('.ul-nav-amazing-work');
const tabsPicturesAmazing = document.querySelectorAll('.tab-picture');

tabNavAmazing.addEventListener('click', function (event) {

    const tabMenu = document.querySelector('.active-tab-amazing');
    tabMenu.classList.remove('active-tab-amazing');
    tabMenu.classList.remove('start-decor');
    // tabMenu.classList.remove('tabs-onclick');
    tabsPicturesAmazing.forEach(item => {

        item.classList[item.dataset.picture === event.target.dataset.tabamasing
        &&
        item.dataset.allpics === item.dataset.picture ? 'add' : 'remove']('active-pics');

        item.classList[item.dataset.picture === event.target.dataset.tabamasing ? 'remove' : 'add']('hide');

        item.classList[item.dataset.allpics === event.target.dataset.tabamasing ? 'add' : 'remove']('when-page-downloaded');

        if (item.classList.contains('when-page-downloaded')) {
            item.classList.remove('hide')
        }
    });

    event.target.classList.add('active-tab-amazing');
    console.log(event.target.dataset.tabamasing)
    if (event.target.dataset.tabamasing !== "all") {
        loadMoreAmazing.classList.add('hide-button');
    } else {
        loadMoreAmazing.classList.remove('hide-button');
    }
});


// ---   LOAD MORE BUTTON   --- //
const showMorebtn = document.querySelectorAll('.load-more-all-picks')
const tabsPictureDiv =document.querySelector('.tabs-pictures');
const loadMoreAmazing = document.querySelector('.load-more');
loadMoreAmazing.addEventListener('click', function(event) {
    showMorebtn.forEach(item=>{
        // item.classList.contains('load-more-all-picks');
        if (item.dataset.allpics !== null) {
            item.classList.remove('hide');
        }
        item.classList[item.dataset.picture === event.target.dataset.tabamasing ? 'add' : 'remove']('all-tags', 'hide-button');
        // item.classList[item.dataset.picture === event.target.dataset.tabamasing ? 'remove' : 'add']('hide');
        // if () {}
    });
    loadMoreAmazing.classList.add('hide-button');
});



// ---   ---   ---   WHAT PEOPLE SAYS SLIDER  ---   ---   --- //
let currentImg = 0;
const showDivvv = document.querySelectorAll('.slider-div');
const allBlock = document.querySelectorAll('.small-portrets-wrapper');
const wrapper = document.querySelector('.small-portrets-wrapper');
wrapper.addEventListener('click', event => {
    console.log(event.target);
    if (event.target.classList.contains('small-portrets-wrapper')) {
        return;
    } else {
        const tabMenu = document.querySelector('.active-icon');
        tabMenu.classList.remove('active-icon');
        allBlock.forEach(item => {
            showDivvv.forEach((elem, index) => {
                if (event.target.dataset.smaller === elem.dataset.activediv) {
                    currentImg = index;
                    return showDivvv[index].classList.remove('hide');
                }
                showDivvv[index].classList.add('hide');
            });
        });
        event.target.classList.add('active-icon');
    }
});


// ---   ---   ---   WHAT PEOPLE SAYS BUTTONS  ---   ---   --- //

const imgContent = document.querySelectorAll('.small-portrets');

// let activePar = document.querySelectorAll('.slider-div');

const leftButton = document.querySelector('.left-button');


leftButton.onclick = function () {
    imgContent[currentImg].classList.remove('active-icon','active-icon-left','active-icon-right');
    showDivvv[currentImg].classList.add('hide');

    //------------------------//
    if (currentImg === 0) {
        currentImg = imgContent.length;
    }
    currentImg = (currentImg - 1) % imgContent.length;
    //------------------------//

    imgContent[currentImg].classList.add('active-icon','active-icon-left');
    showDivvv[currentImg].classList.remove('hide');
}

const rightButton = document.querySelector('.right-button');
rightButton.onclick = function () {
    imgContent[currentImg].classList.remove('active-icon','active-icon-left','active-icon-right');
    showDivvv[currentImg].classList.add('hide');

    //------------------------//
    currentImg = (currentImg + 1) % imgContent.length;
    //------------------------//

    imgContent[currentImg].classList.add('active-icon','active-icon-right');
    showDivvv[currentImg].classList.remove('hide');
}
